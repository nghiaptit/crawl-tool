import yaml


def get_config_all():
    path = r'configs/config.yaml'
    with open(path, "r") as stream:
        app_config = yaml.safe_load(stream)
    return app_config


def get_config_value(path):
    data = get_config_all()
    keys = path.split('.')
    value = data
    for k in keys:
        if isinstance(value, dict):
            if k == "os":
                value = value.get(k).get(data["os"])
            else:
                value = value.get(k)
        else:
            return None
    return value
