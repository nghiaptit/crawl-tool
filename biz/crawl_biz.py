import logging

import pandas as pd

from services.vietstock_crawler import VietStockCrawler
from utils import util

logger = logging.getLogger(__name__)


class CrawlBiz:
    def __init__(self):
        logger.info("-----------------Init Crawl----------------------")
        self.INPUT_FILE = util.get_config_value("input_file")

    def run(self):
        datafile = self.load_file_input()

        data_array = []
        for index, row in datafile.iterrows():
            item_crawl = {'ordinal_num': row.iloc[0], 'stock_code': row.iloc[1], 'stock_name': row.iloc[2],
                          'category': row.iloc[3], 'stock_exchange': row.iloc[4], 'volume': row.iloc[5], }
            data_array.append(item_crawl)

        logger.info(f"Preparing crawl for {len(data_array)} items")
        crawler = VietStockCrawler()
        crawler.crawl_all(data_array)

    def load_file_input(self):
        # Read the Excel file into a pandas DataFrame
        df = pd.read_excel(self.INPUT_FILE)
        logger.info(f"Reading data from {self.INPUT_FILE}""")
        logger.info(f"Total row in file: {len(df.index)}")
        return df
