import logging
import pickle
import time

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chromium import webdriver
from selenium.webdriver.common.by import By

logger = logging.getLogger(__name__)

from utils import util


class LoginBiz:

    def __init__(self):
        options = Options()
        # options.add_argument('--headless')
        options.binary_location = util.get_config_value("config.os.path.browser_path")
        service = Service(executable_path=util.get_config_value("config.os.path.driver_path"))

        self.driver = webdriver.ChromiumDriver(options=options, service=service)

        # Navigate to the URL
        url = "https://finance.vietstock.vn/MBB/tai-chinh.htm?tab=BCTT"  # Replace this with your desired URL
        self.driver.get(url)
        logger.info(f"Go to the {url} page")

        auto_login = util.get_config_value("credential.auto")
        wait_time = util.get_config_value("credential.wait_time")
        if auto_login:
            self.auto_login()
        else:
            logger.info(f"Wait for {wait_time} seconds to manual login !")
            time.sleep(int(wait_time))
        # save cookies
        self.save_cookies()
        logger.info("Login Biz success")

    def save_cookies(self):
        # After successful login, extract cookies
        cookies = self.driver.get_cookies()

        # Save cookies to a file
        with open('cookies.pkl', 'wb') as f:
            pickle.dump(cookies, f)

    def auto_login(self):
        self.driver.find_element(By.CLASS_NAME, "btnlogin-link").click()
        # time.sleep(2)

        # Find the username and password fields by their name attributes
        username_field = self.driver.find_element(By.ID, 'txtEmailLogin')
        password_field = self.driver.find_element(By.ID, 'txtPassword')

        user = util.get_config_value("credential.user")
        passwd = util.get_config_value("credential.pwd")

        # Enter your username and password
        username_field.send_keys(user)
        password_field.send_keys(passwd)
        time.sleep(4)

        # Submit the form
        button = self.driver.find_element(By.ID, 'btnLoginAccount')
        button.click()

        time.sleep(5)
