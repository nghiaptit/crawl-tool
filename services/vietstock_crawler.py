import logging
import os
import pickle
import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait

from utils import util

logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger('selenium.webdriver.remote.remote_connection').setLevel(logging.WARNING)
logger = logging.getLogger(__name__)


class VietStockCrawler:
    def __init__(self):
        self.wait = None
        self.driver = None
        self.require_cookies = True

        self.prepare_crawler()
        logger.info("Init crawler")

    def prepare_crawler(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--ignore-certificate-errors-spki-list')
        options.add_argument('--ignore-ssl-errors')
        options.add_argument('log-level=3')
        prefs = {
            "download.default_directory": os.path.abspath(r"output"),
            "download.prompt_for_download": False,
            "download.directory_upgrade": True,
        }
        options.add_experimental_option("prefs", prefs)
        options.binary_location = util.get_config_value("config.os.path.browser_path")
        service = Service(executable_path=util.get_config_value("config.os.path.driver_path"))
        self.driver = webdriver.Chrome(options=options, service=service)
        # Wait for initialize, in seconds
        self.wait = WebDriverWait(self.driver, 10)

    def crawl_all(self, array):
        limit = 30
        err_list = []
        for index, item in enumerate(array):
            result = self.auto_download(item)
            if result == "FAILED":
                err_list.append(item)

            logger.info("Progress: %d/%d" % (index + 1, len(array)))
            if index % limit == 0:
                logger.info("Recreate crawler")
                self.require_cookies = True
                self.prepare_crawler()

        logger.info(f"Finish crawl all. Items err {err_list}")

    # "https://finance.vietstock.vn/MBB/tai-chinh.htm?tab=BCTT"
    def auto_download(self, item_crawl):
        code = item_crawl['stock_code']
        name = item_crawl['stock_name']
        status = "SUCCESS"
        try:
            t1 = time.time()
            url = util.get_config_value("url_template")
            url = url.replace('{companyCode}', code, 1)
            logger.info(f"--> Start request data for {name}-{code} with {url}")
            self.driver.get(url)

            if self.require_cookies:
                #  load cookie
                self.load_cookies()
                self.driver.refresh()
                self.require_cookies = False

            t3 = time.time()
            logger.info(f'<-- Received data for {code} in {round(t3 - t1)} seconds')

            # select for period
            period_selector = self.driver.find_element(By.NAME, 'period')
            Select(period_selector).select_by_value('-1')

            # select for quarter or year
            quarter_selector = self.driver.find_element(By.NAME, 'NumberPeriod')
            report_type = util.get_config_value("report_type")
            if report_type == "YEAR":
                Select(quarter_selector).select_by_value('NAM')
            else:
                Select(quarter_selector).select_by_value('QUY')

            # select for unit
            unit_selector = self.driver.find_element(By.NAME, 'UnitDong')
            Select(unit_selector).select_by_value('1000000000')

            time.sleep(3)

            # select export file
            self.wait.until(EC.visibility_of_element_located((By.ID, 'dropdownMenuButton')))

            btn_download = self.driver.find_element(By.ID, 'dropdownMenuButton')
            btn_download.click()

            excel_xpath = '//*[@id="finance-content"]/div/div/div[2]/div/div[2]/div[1]/div[2]/div/div/a[2]'
            btn_excel = self.driver.find_element(By.XPATH, excel_xpath)
            self.wait.until(EC.visibility_of_element_located((By.XPATH, excel_xpath)))
            ActionChains(self.driver).move_to_element(btn_excel).click(btn_excel).perform()

            t2 = time.time()

            logger.info(f'Downloaded data for {code}: {round(t2 - t1)} seconds')

            time.sleep(5)
        except Exception as e:
            logger.exception(f"Err when crawl {name}-{code}")
            status = "FAILED"

        return status

    def close(self):
        self.driver.quit()

    def load_cookies(self):
        # Load cookies from file
        with open('cookies.pkl', 'rb') as f:
            cookies = pickle.load(f)

        # Add cookies to the browser session
        for cookie in cookies:
            self.driver.add_cookie(cookie)
